import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './GameBoard.css';


const cardTypes = '{"cardTypes":[' +
  '{"name":"Kysymysmestari","desc":"Olet kysymysmestari kunnes joku toinen saa taman kortin! Saat kysya kysymyksia ja' +
  '                        kysymykseen vastannut ottaa ryypyn!" },' +
  '{"name":"Vesiputous","desc":"Vesiputous!" },' +
  '{"name":"Hitler","desc":"Huutakaa Hitler - viimeinen huutaja juo" },' +
  '{"name":"Kategoria","desc":"Paattakaa kategoria ja sanokaa siihen kuuluvia asioita. Kun ei enaa keksi tai sanoo jo aiemmin sanotun asian, joutuu juomaan!" },' +
  '{"name":"Saantö","desc":"Keksi saantö" },' +
  '{"name":"Huora","desc":"Valitse itsellesi huora. Huora juo aina kun sinakin juot!" },' +
  '{"name":"Tytot juo","desc":"Tytot juovat yhden ryypyn!" },' +
  '{"name":"Pojat juo","desc":"Pojat juovat yhden ryypyn!" }]}';
const cardTypesCount = 8;
const playerList = ["Ismo", "Seppo", "Ulla", "Oona", "Maarit"];
const playerCount = playerList.length;
const firstPlayer = playerList[0];
var currentPlayerID = 1;

class gameBoard extends Component {

  constructor(props) {
    super(props)
    // this.handleChange = this.handleChange.bind(this)
    // this.handleRefreshClick = this.handleRefreshClick.bind(this)
  }

  componentDidMount() {
    // const { dispatch, selectedSubreddit } = this.props
    // dispatch(fetchPostsIfNeeded(selectedSubreddit))
  }
  
  static nextCard() {
    document.querySelector(".flip-container").classList.toggle("flip");
    var flipElement = document.getElementById('flipElement');
    if (flipElement.classList.contains('flip')) {
      const randomCardNumber = Math.floor(Math.random() * cardTypesCount);
      const cardName = gameBoard.randomCard(randomCardNumber)[0];
      const cardDesc = gameBoard.randomCard(randomCardNumber)[1];
      document.querySelector(".back-title").innerHTML = cardName;
      document.querySelector(".back-desc").innerHTML = cardDesc;
    } else {
      document.querySelector(".current-player-name").innerHTML = playerList[currentPlayerID];
      if (currentPlayerID === (playerCount - 1)) {
        currentPlayerID = 0;
      } else {
        currentPlayerID += 1;
      }
    }
  }

  static randomCard(n) {
    const card = JSON.parse(cardTypes);
    console.log('> N on: ' + n);
    return([card.cardTypes[n].name, card.cardTypes[n].desc]);
  }

  render() {
    const { selectedSubreddit, posts, isFetching, lastUpdated } = this.props;
    const randomCardNumber = Math.floor(Math.random() * cardTypesCount);
    const cardName = gameBoard.randomCard(randomCardNumber)[0];
    const cardDesc = gameBoard.randomCard(randomCardNumber)[1];
      return (
          <div className="Form">
              <header className="App-header">
                <div className="player-name">
                  On pelaajan <b><span className="current-player-name">{ firstPlayer }</span></b> vuoro
                </div>

                <div id="flipElement" className="flip-container">
                  <div className="flipper" onClick={gameBoard.nextCard}>
                    <div className="front">
                      <img className="card-deck-bg" src={"/card-deck.png"}/>
                    </div>
                    <div className="back">
                      <div className="back-title">{ cardName }</div>
                      <div className="back-desc">{ cardDesc }</div>
                    </div>
                  </div>
                </div>
              </header>

          </div>
      );
  }
}

// function mapStateToProps(state) {
//     const { selectedSubreddit, postsBySubreddit } = state
//     const { isFetching, lastUpdated, items: posts } = postsBySubreddit[
//       selectedSubreddit
//     ] || {
//       isFetching: true,
//       items: []
//     }

//     return {
//       selectedSubreddit,
//       posts,
//       isFetching,
//       lastUpdated
//     }
//   }
  
// export default connect(mapStateToProps)(gameBoard)
export default gameBoard