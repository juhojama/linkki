import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>LINKKI</h1>
          <p>
            The Drinking Game
          </p>
          <a href={"/names"}>
            Start the Game!
          </a>
        </header>
      </div>
    );
  }
}

export default App;
