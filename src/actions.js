export const FETCHING_DATA = 'FETCHING_DATA'
export const FETCH_SUCCESS = 'FETCH_DATA_SUCCESS'
export const CARD_ERROR = 'CARD_ERROR'
export const CARD_LOADING = 'CARD_LOADING'

export function fetch(data) {
  return {
    type: FETCHING_DATA,
    data
  }
}

export function cardHasError(bool) {
    return {
        type: 'CARD_ERROR',
        hasErrored: bool
    };
}

export function cardIsLoading(bool) {
    return {
        type: 'CARD_LOADING',
        isLoading: bool
    };
}

export function FetchDataSuccess(cards) {
    return {
        type: 'FETCH_DATA_SUCCESS',
        cards
    };
}


