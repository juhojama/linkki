import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import gameBoard from './gameBoard'
import names from './names'

import configureStore from './configureStore'

const store = configureStore();

const routing = (
  <Router>
    <div>
        <Route exact path="/" component={App} />
        <Route path="/game" component={gameBoard} />
        <Route path="/names" component={names} />
    </div>
  </Router>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
