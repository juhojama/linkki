import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './App.css';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    root: {
        width: '50%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
    id += 1;
    return { id, name, calories, fat, carbs, protein };
}

const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
];

class names extends Component {
  constructor(props) {
    // this.handleChange = this.handleChange.bind(this)
    // this.handleRefreshClick = this.handleRefreshClick.bind(this)
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
}

  componentDidMount() {
    // const { dispatch, selectedSubreddit } = this.props
    // dispatch(fetchPostsIfNeeded(selectedSubreddit))
  }

  handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

  render() {
    const { selectedSubreddit, posts, isFetching, lastUpdated, classes } = this.props;

      return (
          <div className="Form">
              <header className="App-header">
                  <h1>Pelaajien nimet</h1>
                  <form className={classes.container} noValidate autoComplete="off">
                      <TextField
                          id="outlined-name"
                          label="Nimi"
                          className={classes.textField}
                          value={this.state.name}
                          onChange={this.handleChange('name')}
                          margin="normal"
                          variant="outlined"
                      />
                  </form>

                  <Paper className={classes.root}>
                      <Table className={classes.table}>
                          <TableHead>
                              <TableRow>
                                  <TableCell>#</TableCell>
                                  <TableCell>Nimi</TableCell>
                              </TableRow>
                          </TableHead>
                          <TableBody>
                              {rows.map(row => {
                                  return (
                                      <TableRow key={row.id}>
                                          <TableCell component="th" scope="row">
                                              {row.name}
                                          </TableCell>
                                          <TableCell>{row.name}</TableCell>
                                      </TableRow>
                                  );
                              })}
                          </TableBody>
                      </Table>
                  </Paper>

                {/*Link to the game board*/}
                <a href={"/game"}>
                  Ready!
                </a>

              </header>
          </div>
      );
  }
}

// function mapStateToProps(state) {
//     const { selectedSubreddit, postsBySubreddit } = state
//     const { isFetching, lastUpdated, items: posts } = postsBySubreddit[
//       selectedSubreddit
//     ] || {
//       isFetching: true,
//       items: []
//     }

//     return {
//       selectedSubreddit,
//       posts,
//       isFetching,
//       lastUpdated
//     }
//   }
  
// export default connect(mapStateToProps)(gameBoard)
names.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(names);
